package com.taeschma.binary.search;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Node {
    private String lieferantName;
    private Node leftChild;
    private Node rightChild;

    public Node(String lieferantName) {
        this.lieferantName = lieferantName;
    }
}
