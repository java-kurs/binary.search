package com.taeschma.binary.search;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class BinarySearchController {

    private BinarySearchTree bst;

    public BinarySearchController() {
        this.bst = new BinarySearchTree();
        this.bst.insert("Hallo Pizza");
        this.bst.insert("007 Pizza");
        this.bst.insert("Pizza Alvin");
        this.bst.insert("Pizza Hölle");
        this.bst.insert("Pizza 0815");
        this.bst.insert("005 Pizza");
        this.bst.insert("Zenit Pizza");
        this.bst.insert("006 Pizza");
        this.bst.insert("004 Pizza");
        this.bst.insert("Asterix Pizza");
    }

    @GetMapping("/show-tree")
    public BinarySearchTree showTree() {
        return this.bst;
    }

    @GetMapping("/sorted-list")
    public List<String> showSortedList() {
        return this.bst.getSortierteLieferantenliste();
    }

    @GetMapping("/search/{lieferantname}")
    public Node searchLieferant(@PathVariable("lieferantname") String search) {
        Node ret = null;
        if(this.bst.getRoot()!=null) {
            ret = this.bst.search(search, this.bst.getRoot());
        }
        return ret;
    }

    @GetMapping("/add/{lieferantname}")
    public String addLieferant(@PathVariable("lieferantname") String add) {
        this.bst.insert(add);
        return add;
    }

    @GetMapping("/delete/{lieferantname}")
    public String deleteLieferant(@PathVariable("lieferantname") String search) {
        if(this.bst.getRoot()!=null) {
            this.bst.deleteRekursion(search, this.bst.getRoot());
        }
        return search;
    }
}
