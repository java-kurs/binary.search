package com.taeschma.binary.search;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ToString
@Getter
@Setter
public class BinarySearchTree {
    private Node root;
    private List<String> sortierteLieferantenliste;

    /**
     * Node Suche
     *
     * @param searchName  Name des Lieferanten
     * @param currentNode Root bzw. Current Noce beim Suchdurchlauf
     * @return
     */
    public Node search(String searchName, Node currentNode) {
        Node ret = null;
        log.info("Beim Test berücksichtigt: " + currentNode.getLieferantName());
        if (searchName.compareTo(currentNode.getLieferantName()) == 0) { // stimmt überein
            log.info("gefunden");
            ret = currentNode;
        } else if (searchName.compareTo(currentNode.getLieferantName()) < 0) { //Linke Seite (die Kleinere)
            if (null != currentNode.getLeftChild()) {
                log.info("links rum");
                ret = this.search(searchName, currentNode.getLeftChild());
            }
        } else { //Rechte Seite (die groessere)
            if (null != currentNode.getRightChild()) {
                log.info("rechts rum");
                ret = this.search(searchName, currentNode.getRightChild());
            }
        }
        return ret;
    }

    /**
     * @param lieferantName Einzufuegender Lieferantenname
     */
    public void insert(String lieferantName) {
        //Baum leer? Wurzel setzen
        if (root == null) {
            root = new Node(lieferantName);
        }
        //sonst Einfuege-Algorithmus
        else {
            insertNode(lieferantName, root); //Einfügevorgang startet immer bei root
        }
        //neu sortieren
        this.traversal();
    }

    /**
     * Richtige Stelle im Baum Suchen und dort den Lieferantennamen einfuegen
     *
     * @param newLieferantName Einzufuegender Lieferantenname
     * @param node             aktuelle Node im Einfuegeprozess
     */
    private void insertNode(String newLieferantName, Node node) {
        /**
         * compareTo
         *      identisch: 0
         *      <0: newLieferantName ist KLEINER als node.Lieferantenname
         *      >0: newLieferantName ist GROESSER als node.Lieferantenname
         */
        if (newLieferantName.compareTo(node.getLieferantName()) < 0) { //Linke Seite (die Kleinere)
            if (node.getLeftChild() != null) {
                //rekursiver Aufruf mit child-node
                insertNode(newLieferantName, node.getLeftChild());
            } else {
                //hier als neue Node einfuegen
                node.setLeftChild(new Node(newLieferantName));
            }
        } else { //Rechte Seite (die groessere)
            if (node.getRightChild() != null) {
                //rekursiver Aufruf mit child-node
                insertNode(newLieferantName, node.getRightChild());
            } else {
                //hier als neue Node einfuegen
                node.setRightChild(new Node(newLieferantName));
            }
        }
    }

    /**
     * Sortieren (z. B. als Vorbereitung für Wertesuche)
     */
    private List<String> traversal() {
        this.sortierteLieferantenliste = new ArrayList<>();
        if (this.root != null) {
            this.inOrderTraversal(this.root);
        }
        return this.sortierteLieferantenliste;
    }

    private void inOrderTraversal(Node node) {
        //Wenn links noch eine Node, dann zu dieser wechseln
        if (node.getLeftChild() != null) {
            inOrderTraversal(node.getLeftChild());
        }

        //Wenn links keine Node mehr, die aktuelle zur sortierten Liste hinzufuegen und...
        this.sortierteLieferantenliste.add(node.getLieferantName());

        //...zur naechsten rechten Node wechseln
        if (node.getRightChild() != null) {
            inOrderTraversal(node.getRightChild());
        }
    }

    /**
     * Loescheneiner Node anstossen
     *
     * @param data Suchstring
     * @param node Node (initialer Aufruf mit root-Node)
     * @return
     */
    public Node deleteRekursion(String data, Node node) {
        if (node == null) {
            return node;
        }
        if (data.compareTo(node.getLieferantName()) < 0) { //nach links
            node.setLeftChild(deleteRekursion(data, node.getLeftChild())); //Rekursiver Aufruf mit linkem Kind
        } else if (data.compareTo(node.getLieferantName()) > 0) { //nach rechts
            node.setRightChild(deleteRekursion(data, node.getRightChild())); //Rekursiver Aufruf mit rechtem Kind
        } else { //gefunden
            log.info("Found node to delete");
            node = handleDeleteMode(node);
        }

        //Geordnete Liste neu berechnen
        this.traversal();

        return node;
    }


    /**
     * Loeschen der Node in Abhängigkeit ob
     * - Blatt
     * - Halbblatt
     * - (sub)Wurzel
     *
     * @param nodeToDelete zu loeschende Node
     * @return
     */
    private Node handleDeleteMode(Node nodeToDelete) {
        //Node ist ein Blatt
        if (nodeToDelete.getLeftChild() == null && nodeToDelete.getRightChild() == null) {
            log.info("Delete leaf node...: " + nodeToDelete.getLieferantName());
            return null;
        }
        //Node ist ein Halbblatt
        if (nodeToDelete.getLeftChild() == null) {
            log.info("Delete the right child...");
            Node tempNode = nodeToDelete.getRightChild();
            nodeToDelete = null;
            return tempNode;
        } else if (nodeToDelete.getRightChild() == null) {
            log.info("Delete the left child...");
            Node tempNode = nodeToDelete.getLeftChild();
            nodeToDelete = null;
            return tempNode;
        }

        //Node ist selber eine Wurzel (Zwei Kinder...)
        log.info("delete node with two children...");
        Node tempNode = getPredecessor(nodeToDelete.getLeftChild());

        nodeToDelete.setLieferantName(tempNode.getLieferantName());
        nodeToDelete.setLeftChild(deleteRekursion(tempNode.getLieferantName(), nodeToDelete.getLeftChild()));

        return nodeToDelete;
    }

    /**
     * der am weitesten rechts stehende Wert im linken Baum
     * dieser wird die neue (Sub)wurzel
     *
     * @param node
     * @return
     */
    private Node getPredecessor(Node node) {
        if (node.getRightChild() != null) {
            return getPredecessor(node.getRightChild());
        }

        log.info("Predecessor: " + node.getLieferantName());
        return node;
    }


}
